// Number of dweets
const MAX = 12280;

// Dweet functions
let frame = 0
const S =  Math.sin
const C =  Math.cos
const T =  Math.tan

function R(r, g, b, a) {
 var a = a === undefined ? 1 : a;
 return "rgba(" + (r|0) + ", " + (g|0) + ", " + (b|0) + ", "+ a +")";
}

function u(t, dweet) {
  let c = document.getElementById("canvas")
  c.width = window.innerWidth;
  c.height = window.innerHeight;
  let x = X = c.getContext("2d");
  eval(dweet)
}

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

async function getDweet(id){
  let response = await fetch(`https://www.dwitter.net/api/dweets/${id}/`)
  if (response.status == 404){
    return getDweet(getRandomID())
  }
  const code = await response.json()
  console.log(code.code)
  return code.code
}


async function run(dweet){
  while(true){
    u(window.performance.now()/1000, dweet)
    //don't kill our pc
    await sleep(50)
  }
}

function getRandomID(){
  return Math.floor(Math.random() * Math.floor(MAX))
}

document.addEventListener('DOMContentLoaded', function() {
  const dweet = getDweet(getRandomID())
  .then(function(dweet) {
     run(dweet);
    })
});
